class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: "Olá Mundo!"
  end
  
  def goodbye
    render html: "Xau! Mundo!"
  end
  
end